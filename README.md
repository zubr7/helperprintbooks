This is helper for printing the multiple pages of books.
Why is it needed:
- there is not integrated in the printer driver printing of the book in the form of a brochure;
- during printing may end ink / toner, or a misfeed. Then you have to type all over again.